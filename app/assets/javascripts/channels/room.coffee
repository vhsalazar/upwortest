App.room = App.cable.subscriptions.create "RoomChannel",
  connected: ->
    console.log('connected')
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    console.log(data)

  update: (data)->
    @perform 'update', data: data, message: data

  speak: (message)->
    @perform 'update', message: message



$(document).on 'keypress', '[data-behavior~=room_speaker]', (event) ->
  if event.keyCode is 13
    App.room.update event.target.value
    event.target.value = ''
    event.preventDefault()
    console.log('data sent')
