class RoomChannel < ApplicationCable::Channel
  def subscribed
    stream_from "room_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    puts "UNSUBSCRIBED"
  end

  def update(data)
    ActionCable.server.broadcast "room_channel", data: data['data']
  end
end
